/*
 * Copyright (c) 2020 Mihail Cherciu
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files
 * (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge,
 * publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
#include <target.h>
#include <dev/gpio.h>
#include <dev/i2c.h>
#include <lib/console.h>
#include <platform/gpio.h>
#include <platform/stm32.h>
#include <platform/usbc.h>
#include <platform/exti.h>
#include <platform/adc.h>
#include <platform/spi.h>
#include <target/gpioconfig.h>
#include <kernel/thread.h>

#define LOG_MODULE_NAME "INIT"
#define DEBUG   (LOG_FATAL | LOG_ERROR | LOG_WARNING | LOG_INFO /* | LOG_DEBUG */ )
#include "log/log.h"

void target_early_init(void)
{
    /* configure status leds */
    gpio_config(GPIO_LED_GREEN,     GPIO_OUTPUT);
    gpio_set(GPIO_LED_GREEN,    0);

    /* configure button */
    gpio_config(GPIO_BTN_USER, GPIO_INPUT);

    /* configure gpio SPI */
    gpio_config(GPIO_SPI1_CS0,       GPIO_OUTPUT);
    gpio_set(GPIO_SPI1_CS0,      1);
    gpio_config(GPIO_SPI1_CS1,       GPIO_OUTPUT);
    gpio_set(GPIO_SPI1_CS1,      1);
    gpio_config(GPIO_SPI1_CS2,       GPIO_OUTPUT);
    gpio_set(GPIO_SPI1_CS2,      1);
    gpio_config(GPIO_SPI2_CS,       GPIO_OUTPUT);
    gpio_set(GPIO_SPI2_CS,      1);

    /* TODO: configure adc on lk*/
    gpio_config(GPIO_ADC0, GPIO_INPUT);
    gpio_config(GPIO_ADC0, GPIO_INPUT);

    /* TODO: pwm implementation */

    /* configure share interface */
    /* I2C1 */
    gpio_config(GPIO_I2C1_SDA,  GPIO_STM32_AF | GPIO_STM32_AFn(1));
    gpio_config(GPIO_I2C1_CLK,  GPIO_STM32_AF | GPIO_STM32_AFn(1));
    /* I2C2 */
    gpio_config(GPIO_I2C2_SDA,  GPIO_STM32_AF | GPIO_STM32_AFn(5));
    gpio_config(GPIO_I2C2_CLK,  GPIO_STM32_AF | GPIO_STM32_AFn(5));

    /* SPI1 */
    gpio_config(GPIO_SPI1_SCK,  GPIO_STM32_AF | GPIO_STM32_AFn(0));
    gpio_config(GPIO_SPI1_MISO, GPIO_STM32_AF | GPIO_STM32_AFn(0));
    gpio_config(GPIO_SPI1_MOSI, GPIO_STM32_AF | GPIO_STM32_AFn(0));
    /* SPI2 */
    gpio_config(GPIO_SPI2_SCK,  GPIO_STM32_AF | GPIO_STM32_AFn(0));
    gpio_config(GPIO_SPI2_MISO, GPIO_STM32_AF | GPIO_STM32_AFn(0));
    gpio_config(GPIO_SPI2_MOSI, GPIO_STM32_AF | GPIO_STM32_AFn(0));

    /* configure the usart1 pins - console uart*/
    gpio_config(GPIO_UART1_TX, GPIO_STM32_AF | GPIO_STM32_AFn(1) | GPIO_PULLUP);
    gpio_config(GPIO_UART1_RX, GPIO_STM32_AF | GPIO_STM32_AFn(1) | GPIO_PULLUP);

    /* configure the usart3 pins - can uart*/
    gpio_config(GPIO_UART3_TX, GPIO_STM32_AF | GPIO_STM32_AFn(4) | GPIO_PULLUP);
    gpio_config(GPIO_UART3_RX, GPIO_STM32_AF | GPIO_STM32_AFn(4) | GPIO_PULLUP);

    stm32_debug_early_init();
    i2c_init_early();
}


void target_init(void)
{
    int ret;

    thread_sleep(500);
    stm32_debug_init();
    /* TODO: make it configurable, like I2C in rules.mk to select which SPI to use*/
    i2c_init();
    spi_init(SPI_DATA_SIZE_8,
             SPI_CPOL_LOW,
             SPI_CPHA_0,
             SPI_BIT_ORDER_MSB,
             SPI_PRESCALER_8);

    ret = adc_init();
    print_info("ADC device init :%d\n", ret);
    thread_sleep(500);
}

