MODULES += \
	app/system \
	app/shell \
	app/leds \
	app/buttons \
	app/eeprom \
	app/flash \
	lib/version \
	lib/log \
	lib/cmd

include project/target/izirunf0.mk

GLOBAL_INCLUDES += $(LOCAL_DIR)/include
