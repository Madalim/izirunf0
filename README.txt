How to build and flash (linux):
1.  Install cross compiler:
    Binutils:
        sudo apt-get install binutils-arm-none-eabi

    GCC Arm cross-compiler
        sudo apt-get install gcc-arm-none-eabi

    GDB debugger:
        sudo apt-get install gdb-multiarch

2. Clone LK form IZITRON repo
    git clone https://izitron@bitbucket.org/izi_team/lk.git -b izitron

3. Clone izirunf0 from IZITRON repo
    git clone https://izitron@bitbucket.org/izi_team/izirunf0.git
    cd izirunf0
    make

4. Flash lk
    Using ST-LINK
        Install stlink
            git clone https://github.com/texane/stlink stlink.git
            cd stlink
            make
            #install binaries:
            sudo cp build/Release/st-* /usr/local/bin
            #install udev rules
            sudo cp etc/udev/rules.d/49-stlinkv* /etc/udev/rules.d/
            #and restart udev
            sudo udevadm control --reload

        Flash lk.bin
            st-flash write build-izirunf0_izigoboard/lk.bin 0x8000000

    Using UART
        Install stm32flash-code
            git clone https://git.code.sf.net/p/stm32flash/code stm32flash-code
            cd stm32flash-code
            make
            make install

        Flash lk.bin
            stm32flash -w build-izirunf0_izigoboard/lk.bin -v  /dev/ttyUSBx
