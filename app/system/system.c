/*
 * Copyright (c) 2019-2020 IZITRON (Mihail Cherciu)
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files
 * (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge,
 * publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
#include <app.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <platform.h>
#include <platform/adc.h>
#include <arch/arm/cm.h>
#include <target/gpioconfig.h>
#include <target/sysconfig.h>
#include <kernel/thread.h>
#include <dev/gpio.h>
#include <dev/uart.h>
#include <lk/err.h>
#include "math.h"
#include "system.h"
#include "leds.h"
#include "flash.h"
#include "eeprom.h"

#define LOG_MODULE_NAME "SYSTEM"
#define DEBUG   (LOG_FATAL | LOG_ERROR | LOG_WARNING | LOG_INFO | LOG_DEBUG)
#include "log/log.h"

#define EVENT(x) (sys.cmd.what == (x))

sys_manager_t sys;

thread_t *threadMainSystem;

#if defined(WITH_LIB_CONSOLE)
#include <lib/console.h>

static int system_cmd(int argc, const cmd_args *argv);

STATIC_COMMAND_START
STATIC_COMMAND("system", "system commandes", (console_cmd)&system_cmd)
STATIC_COMMAND_END(comm);

static int system_cmd(int argc, const cmd_args *argv)
{
    if (argc < 3) {
        printf("not enough arguments !\n");
        usage:
            printf("%s mode <test/demo/bike>\n", argv[0].str);
            goto out;
    }

    if (!strcmp(argv[1].str, "mode")) {
        if (!strcmp(argv[2].str, "test")) {

        }
        else if (!strcmp(argv[2].str, "demo")) {

        }
        else if (!strcmp(argv[2].str, "bike")) {

        }
        else {
            goto usage;
        }
    }
    else {
        goto usage;
    }

    out:
        return 0;
}

#endif

void send_char(int port, char c)
{
    uart_putc(port,c);
}

void send_string(int port, const char *str)
{
    while (*str != '\0')
    {
        send_char(port, *str);
        str++;
    }
}

static int main_system(void *arg) {
    lk_time_t cur_time, send_gsm_detect, send_system_info ;
    send_gsm_detect = send_system_info = cur_time  = current_time();
    send_gsm_detect = send_gsm_detect - 60000UL;    /* need for the first gsm sent */

    //acc_cmd(ACC_CMD_ON);

    while (true) {
        cur_time = current_time();

        /* DO SOMETING */

        thread_sleep(1000);
    }
    return 0;
}

static void system_init(const struct app_descriptor *app)
{
    /* Init system */
    print_debug("Init System Manager...\n");
    /* Init acc event */
    event_init(&sys.event, false, 0);

    threadMainSystem = thread_create("system thread", &main_system, NULL, HIGH_PRIORITY+1, 2048);
}

static void system_entry(const struct app_descriptor *app, void *args)
{
    thread_set_priority(HIGH_PRIORITY);

    sys_cmd(SYS_CMD_INIT);

    while (event_wait(&sys.event) == NO_ERROR) {
        print_debug("SYS event: %d\n", sys.cmd.what);

        if (EVENT(SYS_CMD_INIT)) {
            print_debug("SYS event: SYS_CMD_INIT\n");
            thread_resume(threadMainSystem);
            event_unsignal(&sys.event);
        }
        else {
            print_warning("Unknown command !\n");
        }
    }
}

APP_START(system)
    .init   = system_init,
    .entry  = system_entry,
APP_END

int sys_cmd(system_actions_t action)
{
    send_cmd(action, &sys.cmd, &sys.event);

    return 0;
}
