#ifndef SYSTEM_H_
#define SYSTEM_H_

#include "cmd/cmd.h"
#include <kernel/timer.h>

/* SYSTEM commands */
typedef enum {
    SYS_CMD_NONE = 0,
    SYS_CMD_INIT
} system_actions_t;

/* SYSTEM state */
typedef enum {
    SYS_NOT_READY,
    SYS_READY,
    SYS_SLEEPING
} system_states_t;

typedef struct {
    event_t       event;
    spin_lock_t   lock;
    cmd_t         cmd;
} sys_manager_t;

int sys_cmd(system_actions_t action);

void send_char(int port, char c);
void send_string(int port, const char *str);

#endif  // SYSTEM_H_
