/*
 * Copyright (c) 2020 Mihail CHERCIU
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files
 * (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge,
 * publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
#include <app.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <platform.h>
#include <platform/spi.h>
#include <platform/exti.h>
#include <target/gpioconfig.h>
#include <target/sysconfig.h>
#include <kernel/thread.h>
#include <dev/gpio.h>
#include <dev/uart.h>
#include <lk/err.h>
#include "math.h"
#include "system.h"
#include "flash.h"

#define LOG_MODULE_NAME "FLASH"
#define DEBUG   (LOG_FATAL | LOG_ERROR | LOG_WARNING | LOG_INFO  | LOG_DEBUG)
#include "log/log.h"

flash_device_t flash;

#if defined(WITH_LIB_CONSOLE)
#include <lib/console.h>

static int flash_console(int argc, const cmd_args *argv);

STATIC_COMMAND_START
STATIC_COMMAND("flash", "manage flash", (console_cmd)&flash_console)
STATIC_COMMAND_END(flash);

static int flash_console(int argc, const cmd_args *argv)
{
    if (argc < 2) {
usage:
        printf("not enough arguments !\n");
        printf("%s <read/write> <addr mem> <value>\n", argv[0].str);
        goto out;
    }

    if (!strcmp(argv[1].str, "write")) {
        if (argc < 4) goto usage;
        print_debug("FLASH write\n");

    }
    else if (!strcmp(argv[1].str, "read")) {
        if (argc < 3) goto usage;
        print_debug("FLASH read\n");
    }
    else
    {
        goto usage;
    }

out:
    return 0;
}

#endif

void device_select_flash(void) {
    gpio_set(GPIO_SPI2_CS, 0);
}

void device_deselect_flash(void) {
    gpio_set(GPIO_SPI2_CS, 1);
}


static uint8_t write_to_flash(uint8_t address, uint8_t value) {
    uint8_t tx_buffer[2];
    uint8_t rx_buffer[2];

    tx_buffer[0] = address;
    tx_buffer[1] = value;

    device_select_flash();
    spi_xfer(tx_buffer, rx_buffer, 2);
    device_deselect_flash();

    return rx_buffer[1];
}

static uint8_t read_from_flash(uint8_t instr) {
    uint8_t tx_buffer[4];
    uint8_t rx_buffer[4];

    tx_buffer[0] = instr;
    tx_buffer[1] = 0x00;
    tx_buffer[2] = 0x00;
    tx_buffer[3] = 0x00;

    device_select_flash();
    spi_xfer(tx_buffer, rx_buffer, 4);
    device_deselect_flash();

    print_debug("flash: 0x%02x,0x%02x,0x%02x,0x%02x\n", rx_buffer[0], rx_buffer[1], rx_buffer[2], rx_buffer[3]);

    return rx_buffer[1];
}

uint8_t get_flash_id(void) {
    uint8_t id_flash;

    id_flash = read_from_flash(0x9F);
    return id_flash;
}

static void flash_init(const struct app_descriptor *app)
{
    print_debug("Init Flash...\n");
    /* Init flash event */
}

APP_START(flash)
.init   = flash_init,
APP_END
