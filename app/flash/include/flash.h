#ifndef FLASH_H_
#define FLASH_H_

#include "cmd/cmd.h"
#include <kernel/timer.h>

#define LIS2DH_I_AM             0x33



typedef struct {
    bool    init;

} flash_device_t;

uint8_t get_flash_id(void);

#endif  // FLASH_H_
