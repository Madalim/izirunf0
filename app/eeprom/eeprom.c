/*
 * Copyright (c) 2020 Mihail CHERCIU
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files
 * (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge,
 * publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
#include <app.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <platform.h>
#include <target/gpioconfig.h>
#include <target/sysconfig.h>
#include <kernel/thread.h>
#include <dev/gpio.h>
#include <dev/uart.h>
#include <dev/i2c.h>
#include <lk/err.h>
#include "math.h"
#include "system.h"
#include "eeprom.h"

#define LOG_MODULE_NAME "EEPROM"
#define DEBUG   (LOG_FATAL | LOG_ERROR | LOG_WARNING | LOG_INFO  | LOG_DEBUG)
#include "log/log.h"

eeprom_device_t eeprom;

static void write_to_eeprom(eeprom_device_t * device, uint16_t memory_address, const void *data, uint8_t len);
static void read_from_eeprom(eeprom_device_t * device, uint16_t memory_address, void *data, uint8_t len);

#if defined(WITH_LIB_CONSOLE)
#include <lib/console.h>

static int eeprom_console(int argc, const cmd_args *argv);

STATIC_COMMAND_START
STATIC_COMMAND("eeprom", "manage eeprom", (console_cmd)&eeprom_console)
STATIC_COMMAND_END(eeprom);

static int eeprom_console(int argc, const cmd_args *argv)
{
    if (argc < 2) {
usage:
        printf("not enough arguments !\n");
        printf("%s <read/write> <addr mem> <value>\n", argv[0].str);
        goto out;
    }

    if (!strcmp(argv[1].str, "write")) {
        if (argc < 4) goto usage;
        print_debug("EEPROM write\n");
        uint16_t addr = argv[2].u;
        uint8_t value = argv[3].u;
        write_to_eeprom(&eeprom, addr, &value, 1);

    }
    else if (!strcmp(argv[1].str, "read")) {
        if (argc < 3) goto usage;
        print_debug("EEPROM read\n");
        uint16_t addr = argv[2].u;
        uint8_t value = argv[3].u;
        read_from_eeprom(&eeprom, addr, &value, 1);
        print_debug("value:%d\n", value);
    }
    else
    {
        goto usage;
    }

out:
    return 0;
}

#endif

static void write_to_eeprom(eeprom_device_t * device, uint16_t memory_address, const void *data, uint8_t len) {
    int ret;

    eeprom.data[0] = (uint8_t)(memory_address >> 8);
    eeprom.data[1] = (uint8_t)(memory_address & 0x00FF);
    memcpy(&eeprom.data[2], data, len);

    ret = i2c_transmit(device->bus_id, device->i2c_address, eeprom.data, len + 2); // len + 2(byte for memory mapping)
    if (ret) print_error("Write EEPROM fail: %d", ret);
}

static void read_from_eeprom(eeprom_device_t * device, uint16_t memory_address, void *data, uint8_t len) {
    int ret;
    char mem_add[2];

    mem_add[0] = (uint8_t)(memory_address >> 8);;
    mem_add[1] = (uint8_t)(memory_address & 0x00FF);
    ret = i2c_transmit(device->bus_id, device->i2c_address, mem_add, 2);                // set pointer in eeprom address wher to read data
    if (ret) print_error("Write EEPROM fail: %d", ret);

    ret = i2c_receive(device->bus_id, device->i2c_address, data, len);
    if (ret) print_error("Read EEPROM fail: %d", ret);
}

static void eeprom_init(const struct app_descriptor *app)
{
    print_debug("Init EEPROM...\n");
    /* Init eeprom event */
    eeprom.state       = EEPROM_INIT;
    eeprom.bus_id      = EEPROM_BUS;
    eeprom.i2c_address = EEPROM_I2C_ADDRESS;
}

APP_START(eeprom)
    .init   = eeprom_init,
APP_END
