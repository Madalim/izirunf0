#ifndef EEPROM_H_
#define EEPROM_H_

#include "cmd/cmd.h"
#include <kernel/timer.h>

#define EEPROM_BUS              1
#define EEPROM_I2C_ADDRESS      0x50
#define EEPROM_BUF_SIZE         128

/***********************************************
 * EEPROM Mapping
 ************************************************/
#define EEPROM_MEM_SOFT_VERSION        0x0010

/***********************************************
* EEPROM Registers
************************************************/
// TODO create the eeprom register if necesary
// or keep theme as default

/* EEPROM state */
typedef enum {
    EEPROM_OFF,
    EEPROM_INIT,
    EEPROM_ERROR
} eeprom_states_t;

typedef struct {
    eeprom_states_t state;
    uint8_t         bus_id;
    uint8_t         i2c_address;
    uint8_t         data[EEPROM_BUF_SIZE];
} eeprom_device_t;

#endif  // EEPROM_H_
