/****************************************************************************************
 * log.c
 *
 *  Created on: May 19, 2018
 *      Author: Mihail CHERCIU (mcherciu@gmail.com)
 ****************************************************************************************/


/****************************************************************************************
 * Included Files
 ****************************************************************************************/
#include <stdio.h>
#include <string.h>
#include <stdarg.h>
#include <kernel/spinlock.h>
#include <platform.h>
#include <app.h>
#include "log/log.h"

/****************************************************************************************
 * Pre-processor Definitions
 ****************************************************************************************/
#define DEBUG_STRING_SIZE                   1024


/****************************************************************************************
 * Private Data
 ****************************************************************************************/

static const char * pc_str_fatal            = "FATAL";
static const char * pc_str_error            = "ERROR";
static const char * pc_str_warning          = "WARNING";
static const char * pc_str_info             = "INFO";
static const char * pc_str_debug            = "DEBUG";

/**
 *  @brief ANSI Escape sequences used to color text on output console
 */
static const char * pc_str_colorRed         = "\x1b[31m";
static const char * pc_str_colorGreen       = "\x1b[32m";
static const char * pc_str_colorYellow      = "\x1b[33m";
static const char * pc_str_colorBleu        = "\x1b[94m";
static const char * pc_str_colorMagenta     = "\x1b[35m";
static const char * pc_str_typeBold         = "\x1b[1m";
static const char * pc_str_colorNone        = "\x1b[0m";

static char p_buffer[DEBUG_STRING_SIZE];

/****************************************************************************************
 * Public Functions
 ****************************************************************************************/
void print_dbg( uint8_t         log_level,
                const char *    module_name,
                const char *    file_name,
                uint16_t        line_nr,
                const char *    format, ...)
{
    va_list         args;
    const char *    pc_level_string;
    const char *    pc_level_color;
    char *          p_print_string;
    int16_t         current_ch_pos,total_ch_pos;

    memset(p_buffer, 0, sizeof(p_buffer));

    switch (log_level)
    {
        case LOG_FATAL:
            pc_level_string = pc_str_fatal;
            pc_level_color  = pc_str_colorRed;
        break;

        case LOG_ERROR:
            pc_level_string = pc_str_error;
            pc_level_color  = pc_str_colorRed;
        break;

        case LOG_WARNING:
            pc_level_string = pc_str_warning;
            pc_level_color  = pc_str_colorYellow;
        break;

        case LOG_INFO:
            pc_level_string = pc_str_info;
            pc_level_color  = pc_str_colorGreen;
        break;

        case LOG_DEBUG:
            pc_level_string = pc_str_debug;
            pc_level_color  = pc_str_colorBleu;
        break;

        default:
            pc_level_string = NULL;
            pc_level_color  = pc_str_colorNone;
        break;
    }

    p_print_string = p_buffer;
    total_ch_pos=current_ch_pos=0;

    /* First payload is the timestemp in magenta color */
    current_ch_pos = snprintf(p_print_string, DEBUG_STRING_SIZE, "%s%10u%s",
                              pc_str_colorMagenta,
                              current_time(),
                              pc_str_colorNone);

    p_print_string  += current_ch_pos;
    total_ch_pos    += current_ch_pos;

    /* Set color/bold text, level, module name, path file, nr. line */
    current_ch_pos = snprintf(p_print_string, DEBUG_STRING_SIZE - total_ch_pos,"%s%s| %s | %s | %s | %u |%s ",
                           pc_level_color,
                           pc_str_typeBold,
                           pc_level_string,
                           module_name,
                           file_name,
                           line_nr,
                           pc_str_colorNone);

    p_print_string  += current_ch_pos;
    total_ch_pos    += current_ch_pos;

    /* Set new text format */
    current_ch_pos = snprintf(p_print_string, DEBUG_STRING_SIZE - total_ch_pos, "%s", pc_level_color);

    p_print_string  += current_ch_pos;
    total_ch_pos    += current_ch_pos;

    /* Print text */
    va_start(args, format);
    current_ch_pos = vsnprintf( p_print_string,
                                DEBUG_STRING_SIZE-total_ch_pos,
                                format,
                                args);
    va_end(args);

    p_print_string  += current_ch_pos;
    total_ch_pos    += current_ch_pos;

    /* Unset color/bold format */
    current_ch_pos = snprintf(p_print_string,DEBUG_STRING_SIZE - total_ch_pos,"%s", pc_str_colorNone);

    total_ch_pos += current_ch_pos;

    printf(p_buffer);
}

static void log_init(const struct app_descriptor *app)
{
    printf("Logs init\n");
}

APP_START(log)
.init = log_init,
APP_END
